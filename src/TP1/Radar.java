package TP1;
// * -- Titouan D'HALLUIN -- * //

public class Radar extends Equipement{
    private double frequence;
    private double longeurOnde;
    private double porteeMaximal;

    public Radar(String adrIP, String adrMAC, Boolean mis_baie, double[] dim_baie, double frequence, double longeurOnde, double porteeMaximal) {
        super(adrIP, adrMAC, mis_baie, dim_baie);
        this.frequence = frequence;
        this.longeurOnde = longeurOnde;
        this.porteeMaximal = porteeMaximal;
    }

    @Override
    public String toString() {
        return super.toString() +  "Radar{" +
                "frequence=" + frequence +
                ", longeurOnde=" + longeurOnde +
                ", porteeMaximal=" + porteeMaximal +
                '}';
    }
}
