package TP1;
// * -- Titouan D'HALLUIN -- * //

public class Camera extends Equipement {
    private String nbImageSeconde;
    private String typeCamera;
    private double resolutionCamera;

    public Camera(String adrIP, String adrMAC, Boolean mis_baie, double[] dim_baie, String nbImageSeconde, String typeCamera, double resolutionCamera) {
        super(adrIP, adrMAC, mis_baie, dim_baie);
        this.nbImageSeconde = nbImageSeconde;
        this.typeCamera = typeCamera;
        this.resolutionCamera = resolutionCamera;
    }

    @Override
    public String toString() {
        return super.toString() + "Camera{" +
                "nbImageSeconde='" + nbImageSeconde + '\'' +
                ", typeCamera='" + typeCamera + '\'' +
                ", resolutionCamera=" + resolutionCamera +
                '}';
    }
}