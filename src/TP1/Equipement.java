package TP1;
// * -- Titouan D'HALLUIN -- * //

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;

public class Equipement {
    private String adrIP;
    private String adrMAC;
    private String nom_const;
    private Boolean mis_baie;
    private double[] dim_baie;
    private Boolean etat;

    public void setEtat(Boolean etat) {
        this.etat = etat;
    }

    public Equipement(String adrIP, String adrMAC, Boolean mis_baie, double[] dim_baie){
        this.adrIP = adrIP;
        this.adrMAC = adrMAC;
        this.mis_baie = mis_baie;
        this.dim_baie = dim_baie;
        this.etat = false;
        this.nom_const = this.recup_nomconst();
    }
    public String recup_nomconst(){
        String result = null;
        String nomPage = "http://api.macvendors.com/" + this.adrMAC;
        try {
            URL u = new URL(nomPage);
            URLConnection c = u.openConnection();
            BufferedReader din = new BufferedReader(new InputStreamReader(c.getInputStream()));

            result = din.readLine();
            din.close();
        }
        catch (java.io.FileNotFoundException e){
            result = "Pas de constructeur";
        }
        catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
      return result;
    }

    public Boolean compare_adrmac(String adrMAC){
        return adrMAC.equals(this.adrMAC);
    }

    public Boolean compare_adrIP(String adrIP){
        return adrIP.equals(this.adrIP);
    }

    public void demarrer_Equipement(){
        this.setEtat(true);
    }

    public void eteindre_Equipement(){
        this.setEtat(false);
    }

    @Override
    public String toString() {
        return "Equipement : " +
                "\nadrIP='" + adrIP + '\'' +
                "\n, adrMAC='" + adrMAC + '\'' +
                "\n, nom_const='" + nom_const + '\'' +
                "\n, mis_baie=" + mis_baie +
                "\n, dim_baie=" + Arrays.toString(dim_baie) +
                "\n, etat=" + etat + "\n";
    }

    public String getAdrIP() {
        return adrIP;
    }

    public String getAdrMAC() {
        return adrMAC;
    }
}
