package TP1;
// * -- Titouan D'HALLUIN -- * //

public class BarriereAuto extends Equipement{
    private double tailleBarriere;
    private Boolean etatBarriere;

    public BarriereAuto(String adrIP, String adrMAC, Boolean mis_baie, double[] dim_baie, double tailleBarriere, Boolean etatBarriere) {
        super(adrIP, adrMAC, mis_baie, dim_baie);
        this.tailleBarriere = tailleBarriere;
        this.etatBarriere = etatBarriere;
    }

    @Override
    public String toString() {
        return super.toString() +
                "tailleBarriere=" + tailleBarriere +
                ", etatBarriere=" + etatBarriere +
                '}';
    }
}
