package TP1;

import java.util.ArrayList;

public class Client {
    private ArrayList<Equipement> liste_equipement;
    private ArrayList<Porte> liste_porte;

    public Client(){
        liste_equipement = new ArrayList<Equipement>();
        liste_porte = new ArrayList<Porte>();
    }

    public ArrayList<Equipement> getListe_equipement() {
        return liste_equipement;
    }
    public ArrayList<Porte> getListe_porte() {
        return liste_porte;
    }

    public void ajouterEquipement(Equipement e){
        liste_equipement.add(e); //champ pas vide ????
    }
    public void ajouterPorte(Porte e){
        liste_porte.add(e); //champ pas vide ????
    }
    public Boolean rechercherEquipement(String adr, Boolean est_ip, Boolean all){
        if (est_ip){
            return compare_adrIP(adr, all);
        }
        else {
            return compare_adrMAC(adr, all);
        }
    }
    public Boolean compare_adrIP(String adrIP, Boolean cible){
        if (cible){
            // Sur liste_equipement de la class Client
            for(Equipement equipement : liste_equipement){
                if (equipement.getAdrIP().equals(adrIP))
                    return true;
            }
            // Sur chaque Porte
            for(Porte porte : liste_porte){
                for (Equipement equipement : porte.getListe_equipement_porte()){
                    if (equipement.getAdrIP().equals(adrIP))
                        return true;
                }
            }
        }
        else{
            // Sur liste_equipement de la class Client
            for(Equipement equipement : liste_equipement){
                if (equipement.getAdrIP().equals(adrIP))
                    return true;
            }
        }
        return false; // si on a pas trouvé
    }
    public Boolean compare_adrMAC(String adrMAC, Boolean cible){
        if (cible){
            // Sur liste_equipement de la class Client
            for(Equipement equipement : liste_equipement){
                if (equipement.getAdrMAC().equals(adrMAC))
                    return true;
            }
            // Sur chaque Porte
            for(Porte porte : liste_porte){
                for (Equipement equipement : porte.getListe_equipement_porte()){
                    if (equipement.getAdrMAC().equals(adrMAC))
                        return true;
                }
            }
        }
        else{
            // Sur liste_equipement de la class Client
            for(Equipement equipement : liste_equipement){
                if (equipement.getAdrMAC().equals(adrMAC))
                    return true;
            }
        }
        return false; // si on a pas trouvé
    }

    public static void main(String[] args) throws InterruptedException {
        Client c = new Client();
        double[] tab = {12.5, 15.9};
        Equipement eq1 = new Equipement("192.1.1.1", "00:11:43:00:00:01", true, tab);
        Thread.sleep(1000); // permet de ne pas se faire jeter par l'api
        Equipement eq2 = new Equipement("192.8.1.1", "00:11:43:00:00:01", false, tab);
        Thread.sleep(1000);
        Equipement eq3 = new Equipement("192.8.1.144", "00:11:43:00:00:01", false, tab);

        c.ajouterEquipement(eq1);
        c.ajouterEquipement(eq2);

        Porte porte = new Porte(true, "sens");
        porte.ajouterEquipement(eq3);

        c.ajouterPorte(porte);

        InterfaceGraphique GUI = new InterfaceGraphique(c);
    }
}
