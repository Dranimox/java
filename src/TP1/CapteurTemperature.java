package TP1;
// * -- Titouan D'HALLUIN -- * //

public class CapteurTemperature extends Equipement{
    private double temperatureMin;
    private double temperatureMax;
    private double temperatureAct;

    public CapteurTemperature(String adrIP, String adrMAC, Boolean mis_baie, double[] dim_baie, double temperatureMin, double temperatureMax, double temperatureAct) {
        super(adrIP, adrMAC, mis_baie, dim_baie);
        this.temperatureMin = temperatureMin;
        this.temperatureMax = temperatureMax;
        this.temperatureAct = temperatureAct;
    }

    @Override
    public String toString() {
        return super.toString() +  "CapteurTemperature{" +
                "temperatureMin=" + temperatureMin +
                ", temperatureMax=" + temperatureMax +
                ", temperatureAct=" + temperatureAct +
                '}';
    }
}
