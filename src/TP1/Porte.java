package TP1;
// * -- Titouan D'HALLUIN -- * //

import java.util.ArrayList;

public class Porte {
    public ArrayList<Equipement> getListe_equipement_porte() {
        return liste_equipement_porte;
    }

    private Boolean etat_porte;
    private String sens;
    private ArrayList<Equipement> liste_equipement_porte;

    public Porte(Boolean etat_porte, String sens) {
        this.etat_porte = etat_porte;
        this.sens = sens;
        this.liste_equipement_porte = new ArrayList<Equipement>();
    }

    public void ajouterEquipement(Equipement e){
        this.liste_equipement_porte.add(e);
    }
}
