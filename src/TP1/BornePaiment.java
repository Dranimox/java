package TP1;
// * -- Titouan D'HALLUIN -- * //

public class BornePaiment extends Equipement{
    private Boolean typeEcran;
    private String resolutionEcran;
    private double tempsDeReponse;

    public BornePaiment(String adrIP, String adrMAC, Boolean mis_baie, double[] dim_baie, Boolean typeEcran, String resolutionEcran, double tempsDeReponse) {
        super(adrIP, adrMAC, mis_baie, dim_baie);
        this.typeEcran = typeEcran;
        this.resolutionEcran = resolutionEcran;
        this.tempsDeReponse = tempsDeReponse;
    }

    @Override
    public String toString() {
        return super.toString() + "BornePaiment{" +
                "typeEcran=" + typeEcran +
                ", resolutionEcran='" + resolutionEcran + '\'' +
                ", tempsDeReponse=" + tempsDeReponse +
                '}';
    }
}
