package TP1;
// * -- Titouan D'HALLUIN -- * //

public class CapteurPression extends Equipement{
    private double pressionMin;
    private double pressionMax;
    private double pressionAct;

    public CapteurPression(String adrIP, String adrMAC, Boolean mis_baie, double[] dim_baie, double pressionMin, double pressionMax, double pressionAct) {
        super(adrIP, adrMAC, mis_baie, dim_baie);
        this.pressionMin = pressionMin;
        this.pressionMax = pressionMax;
        this.pressionAct = pressionAct;
    }

    @Override
    public String toString() {
        return super.toString() +  "CapteurPression{" +
                "pressionMin=" + pressionMin +
                ", pressionMax=" + pressionMax +
                ", pressionAct=" + pressionAct +
                '}';
    }
}
