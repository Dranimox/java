package TP1;
// * -- Titouan D'HALLUIN -- * //

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

public class InterfaceGraphique {
    private JTextArea texte;
    private JTextArea texte2 = new JTextArea();

    private String adrIP;
    private String adrMAC;
    private Boolean mis_baie;
    private final double[] dim_baie = {0.0, 0.0};
    private double tailleBarriere;
    private Boolean etatBarriere;
    private Boolean typeEcran;
    private String resolutionEcran;
    private double tempsDeReponse;
    private String nbImageSeconde;
    private String typeCamera;
    private double resolutionCamera;
    private double pressionMin;
    private double pressionMax;
    private double pressionAtc;
    private double temperatureMin;
    private double TemperatureMax;
    private double TemperatureAct;
    private double frequence;
    private double longueurOnde;
    private double porteeMaximale;
    private double temperatureMax;
    private double temperatureAct;
    private Boolean etat_porte;
    private String sens;
    private ArrayList<Equipement> liste_equipement_porte = new ArrayList<Equipement>();

    public InterfaceGraphique(Client client) {
        this.fenetrePrincipale(client);
    }

    public void fenetrePrincipale(Client client) {
        JFrame frame = new JFrame();
        frame.setBounds(200, 200, 1460, 540);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // MENU BAR //
        JMenuBar menubar = new JMenuBar();
        JMenu peage = new JMenu("Peage");
        JMenuItem addEquipement = new JMenuItem("Ajouter Equipement");
        addEquipement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fenetreAjouterEquipement(client, false);
            }
        });

        JMenuItem addPorte = new JMenuItem("Ajouter Porte");
        addPorte.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fenetrePorte(client);
            }
        });

        JMenuItem quitter = new JMenuItem("Quitter");
        quitter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispose();
            }
        });
        peage.add(addEquipement);
        peage.add(addPorte);
        peage.add(quitter);
        menubar.add(peage);
        //// FIN MENU BAR ////

        //// AFFICHAGE////
        frame.setLayout(new GridLayout(1, 2));
        JPanel affichage = new JPanel();
        affichage.setLayout(new GridLayout(1, 1));
        texte = new JTextArea(setAffichage(client));
        affichage.add(texte);

        JScrollPane scroll = new JScrollPane(texte, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED); // afin de pouvoir scroll
        frame.add(scroll);

        //// RECHERCHE ////
        JPanel PanelRecherche = new JPanel();
        PanelRecherche.setLayout(new GridLayout(3, 3));

        JLabel Recherche = new JLabel("Rechercher par :");
        PanelRecherche.add(Recherche);

        ButtonGroup groupe = new ButtonGroup();
        JRadioButton mac = new JRadioButton("MAC");
        groupe.add(mac);
        PanelRecherche.add(mac);
        JRadioButton ip = new JRadioButton("IP");
        groupe.add(ip);
        PanelRecherche.add(ip);


        JLabel Adresse = new JLabel("Adresse :");
        PanelRecherche.add(Adresse);
        JTextField SaisieAdresse = new JTextField();
        PanelRecherche.add(SaisieAdresse);

        JButton valider = new JButton("Valider");
        PanelRecherche.add(valider);

        JLabel resultat = new JLabel("Resultat : ");
        PanelRecherche.add(resultat);

        JButton supprimer = new JButton("Supprimer");

        PanelRecherche.add(supprimer);

        JCheckBox cible = new JCheckBox("Recherche globale");
        PanelRecherche.add(cible);

        supprimer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SaisieAdresse.setText("");
                cible.setSelected(false);
            }
        });

        valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!SaisieAdresse.getText().equals("")) {
                    resultat.setText("Resultat : \n" + client.rechercherEquipement(SaisieAdresse.getText(), ip.isSelected(), cible.isSelected()).toString());
                }
            }
        });

        frame.add(PanelRecherche);
        frame.setVisible(true);

        frame.setJMenuBar(menubar);
        frame.setVisible(true);
    }

    private String setAffichage(Client client) {
        String texte_affichage = "PEAGE \n" +
                "Affichage des Equipements (" + client.getListe_equipement().size() + ") : \n\n";
        for (Equipement eq : client.getListe_equipement()) {
            texte_affichage += eq.toString();
            texte_affichage += "\n";
        }
        texte_affichage += "\n Affichage des Portes (" + client.getListe_porte().size() + ") : \n\n";
        for (Porte p : client.getListe_porte()) {
            texte_affichage += p.toString();
            texte_affichage += "\n";
        }
        return texte_affichage;
    }

    private String setAffichage2() {
        String texte_affichage = "PORTES \n" +
                "Affichage des Equipements : \n\n";
        for (Equipement eq : liste_equipement_porte) {
            texte_affichage += eq.toString();
            texte_affichage += "\n";
        }
        return texte_affichage;
    }


    public void fenetreAjouterEquipement(Client client, Boolean ProvientDePorte) {
        JFrame fenetre = new JFrame("Ajouter un équipement");
        fenetre.setBounds(200, 200, 500, 250);
        fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        fenetre.setLayout(new GridLayout(6, 2));

        JLabel saisieMAC = new JLabel("Saisir une adresse MAC:");
        JTextField Mac = new JTextField();

        JLabel saisieIP = new JLabel("Saisir une adresse IP:");
        JTextField IP = new JTextField();

        JLabel saisieType = new JLabel("Saisir un type d'équipement:");
        JComboBox<String> typeEquipement = new JComboBox<>();
        typeEquipement.addItem("Selectionner le type d'équipement");
        typeEquipement.addItem("Camera");
        typeEquipement.addItem("Barrière");
        typeEquipement.addItem("Borne de Paiement");
        typeEquipement.addItem("Capteur de pression");
        typeEquipement.addItem("Capteur de température");
        typeEquipement.addItem("Radar");
        typeEquipement.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                fenetreDetails(String.valueOf(e.getItem()));
            }
        });

        JLabel saisieBaie = new JLabel("Mise en baie");
        JPanel Groupe1 = new JPanel();
        Groupe1.setLayout(new GridLayout(1, 2));
        ButtonGroup groupeBouton1 = new ButtonGroup();
        JRadioButton oui = new JRadioButton("Oui");
        groupeBouton1.add(oui);
        oui.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fenetreBaie();
            }
        });


        JRadioButton non = new JRadioButton("Non");
        non.setSelected(true);
        groupeBouton1.add(non);

        JLabel saisieEtat = new JLabel("Etat:");
        JPanel Groupe2 = new JPanel();
        Groupe2.setLayout(new GridLayout(1, 2));
        ButtonGroup groupeBouton2 = new ButtonGroup();
        JRadioButton allume = new JRadioButton("Allumé");
        groupeBouton2.add(allume);
        JRadioButton eteint = new JRadioButton("Eteint");
        groupeBouton2.add(eteint);

        JLabel BoutonValider = new JLabel("Bouton valider ajout équipement");
        JButton boutonValider = new JButton("Valider");
        boutonValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                adrIP = IP.getText();
                adrMAC = Mac.getText();
                mis_baie = oui.isSelected();

                if (!mis_baie) {
                    dim_baie[0] = 0.0;
                    dim_baie[1] = 0.0;
                }

                String x = String.valueOf(typeEquipement.getSelectedItem());
                switch (x) {
                    case "Camera" -> {
                        Camera a = new Camera(adrIP, adrMAC, mis_baie, dim_baie, nbImageSeconde, typeCamera, resolutionCamera);
                        if (ProvientDePorte) {
                            liste_equipement_porte.add(a);
                            texte2.setText(setAffichage2());
                        } else {
                            client.ajouterEquipement(a);
                            texte.setText(setAffichage(client));
                        }
                        fenetre.dispose();
                    }
                    case "Barrière" -> {
                        BarriereAuto a = new BarriereAuto(adrIP, adrMAC, mis_baie, dim_baie, tailleBarriere, etatBarriere);
                        if (ProvientDePorte) {
                            liste_equipement_porte.add(a);
                            texte2.setText(setAffichage2());
                        } else {
                            client.ajouterEquipement(a);
                            texte.setText(setAffichage(client));
                        }
                        fenetre.dispose();

                    }
                    case "Borne de Paiement" -> {
                        BornePaiment a = new BornePaiment(adrIP, adrMAC, mis_baie, dim_baie, typeEcran, resolutionEcran, tempsDeReponse);

                        if (ProvientDePorte) {
                            liste_equipement_porte.add(a);
                            texte2.setText(setAffichage2());
                        } else {
                            client.ajouterEquipement(a);
                            texte.setText(setAffichage(client));
                        }
                        fenetre.dispose();
                    }
                    case "Capteur de pression" -> {
                        CapteurPression a = new CapteurPression(adrIP, adrMAC, mis_baie, dim_baie, pressionMin, pressionMax, pressionAtc);

                        if (ProvientDePorte) {
                            liste_equipement_porte.add(a);
                            texte2.setText(setAffichage2());
                        } else {
                            client.ajouterEquipement(a);
                            texte.setText(setAffichage(client));
                        }
                        fenetre.dispose();
                    }
                    case "Capteur de température" -> {
                        CapteurTemperature a = new CapteurTemperature(adrIP, adrMAC, mis_baie, dim_baie, temperatureMin, temperatureMax, temperatureAct);
                        if (ProvientDePorte) {
                            liste_equipement_porte.add(a);
                            texte2.setText(setAffichage2());
                        } else {
                            client.ajouterEquipement(a);
                            texte.setText(setAffichage(client));
                        }
                        fenetre.dispose();
                    }

                    case "Radar" -> {
                        Radar a = new Radar(adrIP, adrMAC, mis_baie, dim_baie, frequence, longueurOnde, porteeMaximale);

                        if (ProvientDePorte) {
                            liste_equipement_porte.add(a);
                            texte2.setText(setAffichage2());
                        } else {
                            client.ajouterEquipement(a);
                            texte.setText(setAffichage(client));
                        }
                        fenetre.dispose();
                    }
                }

            }
        });

        fenetre.add(saisieMAC);
        fenetre.add(Mac);

        fenetre.add(saisieIP);
        fenetre.add(IP);

        fenetre.add(saisieType);
        fenetre.add(typeEquipement);

        fenetre.add(saisieBaie);
        Groupe1.add(oui);
        Groupe1.add(non);
        fenetre.add(Groupe1);


        fenetre.add(saisieEtat);
        Groupe2.add(allume);
        Groupe2.add(eteint);
        fenetre.add(Groupe2);

        fenetre.add(BoutonValider);
        fenetre.add(boutonValider);


        fenetre.setVisible(true);
    }

    private void fenetreBaie() {
        JFrame fenetre = new JFrame("Details Baie");
        fenetre.setBounds(200, 200, 500, 250);
        fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        fenetre.setLayout(new GridLayout(3, 2));

        JLabel l_ = new JLabel("Longeur : ");
        fenetre.add(l_);
        JTextField l = new JTextField();
        fenetre.add(l);

        JLabel L_ = new JLabel("Largeur : ");
        fenetre.add(L_);
        JTextField L = new JTextField();
        fenetre.add(L);


        JLabel boutonValider_ = new JLabel("Bouton valider ajout équipement");
        fenetre.add(boutonValider_);
        JButton boutonValider = new JButton("Valider");
        boutonValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dim_baie[0] = Double.parseDouble(l.getText());
                dim_baie[1] = Double.parseDouble(L.getText());
                fenetre.dispose();
            }
        });

        fenetre.add(boutonValider);
        fenetre.setVisible(true);
    }

    public void fenetreDetails(String Equipements) {
        switch (Equipements) {
            case "Camera" -> {
                JFrame fenetre = new JFrame("Details Camera");
                fenetre.setBounds(200, 200, 500, 250);
                fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                fenetre.setLayout(new GridLayout(4, 2));

                JLabel saisie_nbImageSeconde = new JLabel("Nombre d'image par seconde:");
                JTextField nbImageSeconde_ = new JTextField();

                JLabel saisie_typeCamera = new JLabel("Type de camera:");
                JTextField typeCamera_ = new JTextField();

                JLabel saisie_resolutionCamera = new JLabel("Résolution de camera:");
                JTextField resolutionCamera_ = new JTextField();

                JLabel BoutonValider = new JLabel("");
                JButton boutonValider = new JButton("Valider");
                boutonValider.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        resolutionCamera = Double.parseDouble(resolutionCamera_.getText());
                        nbImageSeconde = nbImageSeconde_.getText();
                        typeCamera = typeCamera_.getText();
                        fenetre.dispose();
                    }
                });

                fenetre.add(saisie_nbImageSeconde);
                fenetre.add(nbImageSeconde_);

                fenetre.add(saisie_typeCamera);
                fenetre.add(typeCamera_);

                fenetre.add(saisie_resolutionCamera);
                fenetre.add(resolutionCamera_);

                fenetre.add(BoutonValider);
                fenetre.add(boutonValider);

                fenetre.setVisible(true);
            }
            case "Barrière" -> {
                JFrame fenetre = new JFrame("Details Barrière");
                fenetre.setBounds(200, 200, 500, 250);
                fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                fenetre.setLayout(new GridLayout(3, 2));

                JLabel saisie_tailleBarriere = new JLabel("Taille de la barrière :");
                JTextField tailleBarriere_ = new JTextField();

                JLabel saisie_etatBarriere = new JLabel("Etat barrière:");
                JPanel Groupe = new JPanel();
                Groupe.setLayout(new GridLayout(1, 2));
                ButtonGroup groupeBouton = new ButtonGroup();
                JRadioButton fermee = new JRadioButton("Fermée");
                groupeBouton.add(fermee);
                JRadioButton ouvert = new JRadioButton("Ourverte");
                groupeBouton.add(ouvert);


                JLabel BoutonValider = new JLabel("");
                JButton boutonValider = new JButton("Valider");
                boutonValider.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        tailleBarriere = Double.parseDouble(tailleBarriere_.getText());
                        etatBarriere = !fermee.isSelected();
                        fenetre.dispose();
                    }
                });

                fenetre.add(saisie_tailleBarriere);
                fenetre.add(tailleBarriere_);

                fenetre.add(saisie_etatBarriere);
                fenetre.add(Groupe);
                Groupe.add(ouvert);
                Groupe.add(fermee);

                fenetre.add(BoutonValider);
                fenetre.add(boutonValider);

                fenetre.setVisible(true);
            }
            case "Borne de Paiement" -> {
                JFrame fenetre = new JFrame("Details Borne de paiement");
                fenetre.setBounds(200, 200, 500, 250);
                fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                fenetre.setLayout(new GridLayout(4, 2));

                JLabel saisie_typeecran = new JLabel("Ecran tactil:");
                JPanel Groupe = new JPanel();
                Groupe.setLayout(new GridLayout(1, 2));
                ButtonGroup groupeBouton = new ButtonGroup();
                JRadioButton oui = new JRadioButton("Oui");
                groupeBouton.add(oui);
                JRadioButton non = new JRadioButton("Non");
                groupeBouton.add(non);

                JLabel saisie_resolutionEcran = new JLabel("Résolution Ecran:");
                JTextField resolutionEcran_ = new JTextField();

                JLabel saisie_tempsDeReponse = new JLabel("Temps de réponse:");
                JTextField tempsDeReponse_ = new JTextField();


                JLabel BoutonValider = new JLabel("");
                JButton boutonValider = new JButton("Valider");
                boutonValider.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        typeEcran = !non.isSelected();
                        resolutionEcran = resolutionEcran_.getText();
                        tempsDeReponse = Double.parseDouble(tempsDeReponse_.getText());
                        fenetre.dispose();
                    }
                });
                fenetre.add(saisie_typeecran);
                fenetre.add(Groupe);
                Groupe.add(oui);
                Groupe.add(non);

                fenetre.add(saisie_resolutionEcran);
                fenetre.add(resolutionEcran_);

                fenetre.add(saisie_tempsDeReponse);
                fenetre.add(tempsDeReponse_);

                fenetre.add(BoutonValider);
                fenetre.add(boutonValider);

                fenetre.setVisible(true);
            }
            case "Capteur de pression" -> {
                JFrame fenetre = new JFrame("Details capteur de pression");
                fenetre.setBounds(200, 200, 500, 250);
                fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                fenetre.setLayout(new GridLayout(4, 2));

                JLabel saisie_pressionMin = new JLabel("Pression minimum:");
                JTextField pressionMin_ = new JTextField();

                JLabel saisie_pressionMax = new JLabel("Pression maximum:");
                JTextField pressionMax_ = new JTextField();

                JLabel saisie_pressionAct = new JLabel("Pression atmosphérique:");
                JTextField pressionAct_ = new JTextField();

                JLabel BoutonValider = new JLabel("");
                JButton boutonValider = new JButton("Valider");
                boutonValider.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        pressionMin = Double.parseDouble(pressionMin_.getText());
                        pressionMax = Double.parseDouble(pressionMax_.getText());
                        pressionAtc = Double.parseDouble(pressionAct_.getText());
                        fenetre.dispose();
                    }
                });
                fenetre.add(saisie_pressionMin);
                fenetre.add(pressionMin_);

                fenetre.add(saisie_pressionMax);
                fenetre.add(pressionMax_);

                fenetre.add(saisie_pressionAct);
                fenetre.add(pressionAct_);

                fenetre.add(BoutonValider);
                fenetre.add(boutonValider);

                fenetre.setVisible(true);
            }
            case "Capteur de température" -> {
                JFrame fenetre = new JFrame("Details capteur de température");
                fenetre.setBounds(200, 200, 500, 250);
                fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                fenetre.setLayout(new GridLayout(4, 2));

                JLabel saisie_temperatureMin = new JLabel("Température minimum:");
                JTextField temperatureMin_ = new JTextField();

                JLabel saisie_temperatureMax = new JLabel("Température maximum:");
                JTextField temperatureMax_ = new JTextField();

                JLabel saisie_temperatureAct = new JLabel("Température actuelle:");
                JTextField temperatureAct_ = new JTextField();

                JLabel BoutonValider = new JLabel("");
                JButton boutonValider = new JButton("Valider");
                boutonValider.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        temperatureMin = Double.parseDouble(temperatureMin_.getText());
                        temperatureMax = Double.parseDouble(temperatureMax_.getText());
                        temperatureAct = Double.parseDouble(temperatureAct_.getText());
                        fenetre.dispose();
                    }
                });
                fenetre.add(saisie_temperatureMin);
                fenetre.add(temperatureMin_);

                fenetre.add(saisie_temperatureMax);
                fenetre.add(temperatureMax_);

                fenetre.add(saisie_temperatureAct);
                fenetre.add(temperatureAct_);

                fenetre.add(BoutonValider);
                fenetre.add(boutonValider);

                fenetre.setVisible(true);
            }
            case "Radar" -> {
                JFrame fenetre = new JFrame("Details radar");
                fenetre.setBounds(200, 200, 500, 250);
                fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

                fenetre.setLayout(new GridLayout(4, 2));

                JLabel saisie_frequence = new JLabel("Fréquence:");
                JTextField frequence_ = new JTextField();

                JLabel saisie_longueurOnde = new JLabel("Longueur d'onde:");
                JTextField longueurOnde_ = new JTextField();

                JLabel saisie_porteeMaximale = new JLabel("Portée maximale:");
                JTextField porteeMaximale_ = new JTextField();

                JLabel BoutonValider = new JLabel("");
                JButton boutonValider = new JButton("Valider");
                boutonValider.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        frequence = Double.parseDouble(frequence_.getText());
                        longueurOnde = Double.parseDouble(longueurOnde_.getText());
                        porteeMaximale = Double.parseDouble(porteeMaximale_.getText());
                        fenetre.dispose();
                    }
                });
                fenetre.add(saisie_frequence);
                fenetre.add(frequence_);

                fenetre.add(saisie_longueurOnde);
                fenetre.add(longueurOnde_);

                fenetre.add(saisie_porteeMaximale);
                fenetre.add(porteeMaximale_);

                fenetre.add(BoutonValider);
                fenetre.add(boutonValider);

                fenetre.setVisible(true);
            }
        }
    }

    public void fenetrePorte(Client client) {
        JFrame fenetre = new JFrame("Ajouter une porte");
        fenetre.setBounds(1100, 100, 600, 700);
        fenetre.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        fenetre.setLayout(new GridLayout(5, 2));

        JLabel etatPorte = new JLabel("Etat de la porte:");
        JRadioButton boutonEtatPorte = new JRadioButton();

        JLabel sensPorte = new JLabel("Sens de la porte:");
        JPanel Groupe = new JPanel();
        Groupe.setLayout(new GridLayout(1, 2));
        ButtonGroup groupeBouton = new ButtonGroup();
        JRadioButton sens1 = new JRadioButton("Sens1");
        groupeBouton.add(sens1);
        JRadioButton sens2 = new JRadioButton("Sens2");
        groupeBouton.add(sens2);

        JLabel ajoutEquipement = new JLabel("Ajouter un équipement:");
        JButton boutonAjoutEquipement = new JButton("Ajouter");
        boutonAjoutEquipement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fenetreAjouterEquipement(client, true);
            }
        });

        JLabel equipementAjoute = new JLabel(" Liste des équipements ajoutés:");

        JScrollPane scroll = new JScrollPane(texte2);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        JLabel texteValider = new JLabel("");
        JButton boutonValider = new JButton("Valider");
        boutonValider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                etat_porte = boutonEtatPorte.isSelected();
                if (sens1.isSelected()) {
                    sens = "sens1";
                } else {
                    sens = "sens2";
                }
                /// CREATION DE LA PORTE ///
                Porte a = new Porte(etat_porte, sens);
                for (Equipement eq : liste_equipement_porte) {
                    a.ajouterEquipement(eq); // ajout equipement
                }
                fenetre.dispose();
                setAffichage(client);
                liste_equipement_porte.clear();
            }
        });

        fenetre.add(etatPorte);
        fenetre.add(boutonEtatPorte);

        fenetre.add(sensPorte);
        fenetre.add(Groupe);
        Groupe.add(sens1);
        Groupe.add(sens2);

        fenetre.add(ajoutEquipement);
        fenetre.add(boutonAjoutEquipement);

        fenetre.add(equipementAjoute);
        fenetre.add(scroll);

        fenetre.add(texteValider);
        fenetre.add(boutonValider);

        fenetre.setVisible(true);
    }
}
