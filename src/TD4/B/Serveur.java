package TD4.B;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;


class Serveur implements Serializable{
    private ServerSocket ss;

    public Serveur(Etudiant a) throws IOException, ClassNotFoundException {
        ss = new ServerSocket(12800, 5);
        Socket client = ss.accept();
        System.out.println("Connexion reçue2 de : " + client.getInetAddress());

        ObjectInputStream in = new ObjectInputStream(client.getInputStream());
        ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
        System.out.println("Envoi");

        out.writeObject(a);
        out.flush();
        System.out.println(in.readObject());
        out.close();
        client.close();
        ss.close();
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        new Serveur(new Etudiant("Jooooohn", "Doooooe", 23333));
    }

}


