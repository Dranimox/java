package TD4.B;

import java.io.*;
import java.net.Socket;

class Etudiant{
    private String nom;
    private String prenom;
    private int age;
    public Etudiant(String nom, String prenom, int age){
        this.nom = nom;
        this.prenom = prenom;
        this.age = age;
    }
}

class Client implements Serializable{
        Client(Etudiant a) throws IOException, ClassNotFoundException {
            System.out.println("2");
            Socket client = new Socket("127.0.0.1", 12800);
            System.out.println(client.getInetAddress());
            ObjectInputStream in = new ObjectInputStream(client.getInputStream());
            ObjectOutputStream out = new ObjectOutputStream(client.getOutputStream());
            System.out.println("Reception");

            Etudiant b  = (Etudiant) in.readObject();
            System.out.println(b);
            out.writeObject(a);
            out.flush();
            out.close();
            in.close();
            client.close();
        }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        new Client(new Etudiant("John", "Doe", 23));
    }
}