import java.io.*;
import java.net.*;

public class Ex1{
	public Ex1(String nomPage){
		try{
			URL u = new URL(nomPage);
			URLConnection c= u.openConnection();
			System.out.println("Je charge " + nomPage);
			BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
			PrintStream printer = new PrintStream(new FileOutputStream("/home/user/Bureau/resultat.html"));

			String s = "nonnul";
			while(s != null){
				s =reader.readLine();
				System.out.println(".");
				printer.println(s);
			}
			printer.close();
			reader.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void main (String arg[]){
		new Ex1("https://www.univ-rennes1.fr/");
	}
}