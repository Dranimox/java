package TD1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

public class Ex8 extends JFrame implements MouseListener {
    private int nbClic;
    private final JLabel affichageClic;

    public Ex8(){
        this.setBounds(200,200,1000,500);
        this.nbClic = 0;
        Container pane = this.getContentPane();

        affichageClic = new JLabel("nombre de clic : " + nbClic);
        affichageClic.setSize(1000,500);
        affichageClic.addMouseListener(this);
        this.setLayout(new FlowLayout(FlowLayout.CENTER,0,50));
        pane.add(affichageClic);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        nbClic++;

        Random rand = new Random();
        // Java 'Color' class takes 3 floats, from 0 to 1.
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
        Color randomColor = new Color(r, g, b);

        if(nbClic > 5) {
            nbClic = 0;
            JOptionPane.showMessageDialog(this,"Nombre de click atteint");
        }

        affichageClic.setText("nombre de clic : " + nbClic);
        affichageClic.setForeground(randomColor);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public static void main(String[] args) {
        Ex8 fen = new Ex8();
        fen.setVisible(true);
        fen.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}