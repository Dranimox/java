package TD1;

import javax.swing.*;
import java.awt.*;

public class Ex6 extends JFrame {
    public Ex6(){
        this.setBounds(200,200,1000,500);
        Container pane = this.getContentPane();
        pane.add(new JLabel("label1"), BorderLayout.PAGE_START);
        pane.add(new JCheckBox("case a cocher"), BorderLayout.LINE_START);
        pane.add(new JButton("bouton1"), BorderLayout.CENTER);
        pane.add(new JLabel("label2"), BorderLayout.LINE_END);
        pane.add(new JButton("bouton2"), BorderLayout.PAGE_END);

        this.setVisible(true);
    }

    public static void main(String[] args) {
        new Ex6();
    }
}
