package TD1;

import javax.swing.*;
import java.awt.*;

public class ExSupp extends JFrame {
    public ExSupp(){
        this.setBounds(200,200,100,500);
        Container pane = this.getContentPane();
        // HAUT
        JButton menu = new JButton();
        menu.setBackground(Color.red);
        pane.add(menu, BorderLayout.PAGE_START);

        // BAS
        JButton bas = new JButton();
        bas.setBackground(Color.blue);
        pane.add(bas, BorderLayout.PAGE_END);

        // CENTRE
        JPanel centre = new JPanel();
        pane.add(centre, BorderLayout.CENTER);

        centre.setLayout(new GridLayout(1, 2));
        JPanel centre_gauche = new JPanel();
        centre.add(centre_gauche);

        JPanel centre_droite = new JPanel();
        centre.add(centre_droite);

        centre_droite.add(new JTextArea());
        centre_gauche.add(new JButton());
        centre_gauche.add(new JButton());
        centre_gauche.add(new JButton());
        centre_gauche.add(new JButton());

        setVisible(true);
    }

    public static void main(String[] args) {
        new ExSupp();
    }
}
