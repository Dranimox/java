package TD1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class ExCurseur extends JFrame implements MouseListener {

    final private JLabel l;

    public ExCurseur(){
        this.setBounds(200,200,1000,500);
        Container fen = this.getContentPane();

        this.addMouseListener(this);

        l = new JLabel();
        fen.add(l, BorderLayout.PAGE_END);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        this.setVisible(true);
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        l.setText("APPUI en (" + e.getX() +  ","  + e.getY() + ")" );
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        l.setText("RELACHEMENT en (" + e.getX() +  ","  + e.getY() + ")" );
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public static void main(String[] args) {
        new ExCurseur();
    }
}
