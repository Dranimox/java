package TD1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex9 extends JFrame implements ActionListener {
    private JButton b1;
    private JButton b2;
    private JLabel l;

    public Ex9() {
        this.setBounds(200,200,1000,500);
        Container fen = this.getContentPane();

        b1 = new JButton("b1");
        b1.setBackground(Color.blue);
        b1.addActionListener(this);
        fen.add(b1, BorderLayout.LINE_START);

        b2 = new JButton("b2");
        b2.setBackground(Color.green);
        b2.addActionListener(this);
        fen.add(b2, BorderLayout.LINE_END);


        l = new JLabel();
        fen.add(l, BorderLayout.CENTER);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String str = e.getActionCommand();
        System.out.println("Action : " + str);
        l.setText("Action : " + str);

        switch (e.getActionCommand()) {
            case "b1" -> System.out.println("B1 ACTIVE");
            case "b2" -> System.out.println("B2 ACTIVE");
        }

    }

    public static void main(String[] args) {
        new Ex9();
    }
}
