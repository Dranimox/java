package TD1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex10 extends JFrame implements ActionListener {

    private JButton b1;
    private JButton b2;
    private JButton b3;
    private JButton b4;
    private JButton b5;
    private JLabel l;

    public Ex10() {
        this.setBounds(200,200,1000,500);
        Container fen = this.getContentPane();

        b1 = new JButton("1");
        b1.addActionListener(this);
        b1.setBackground(Color.blue);
        fen.add(b1, BorderLayout.PAGE_START);

        b2 = new JButton("2");
        b2.addActionListener(this);
        b2.setBackground(Color.black);
        fen.add(b2, BorderLayout.PAGE_END);

        b3 = new JButton("3");
        b3.addActionListener(this);
        b3.setBackground(Color.red);
        fen.add(b3, BorderLayout.LINE_START);

        b4 = new JButton("4");
        b4.addActionListener(this);
        b4.setBackground(Color.white);

        b5 = new JButton("5");
        b5.addActionListener(this);
        b5.setBackground(Color.gray);
        fen.add(b5, BorderLayout.LINE_END);

        l = new JLabel();
        fen.add(l);

        JPanel centre = new JPanel();
        fen.add(centre, BorderLayout.CENTER);
        centre.setLayout(new GridLayout(1,2));
        centre.add(b4);
        centre.add(l);

        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        l.setText("Button " + e.getActionCommand());
    }

    public static void main(String[] args) {
        new Ex10();
    }
}
