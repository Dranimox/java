package TD1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Ex7 extends JFrame implements MouseListener {
    private int nbClic;
    private JLabel affichageClic;

    public Ex7(){
        this.setBounds(200,200,1000,500);
        this.nbClic = 0;
        Container pane = this.getContentPane();

        affichageClic = new JLabel("nombre de clic : " + nbClic);
        affichageClic.addMouseListener(this);
        pane.add(affichageClic, BorderLayout.CENTER);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        nbClic++;

        if(nbClic >= 5)
            affichageClic.setText("FIN");
        else
            affichageClic.setText("nombre de clic : " + nbClic);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    public static void main(String[] args) {
        Ex7 fen = new Ex7();
        fen.setVisible(true);
    }
}