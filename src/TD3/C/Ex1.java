package TD3.C;

import java.io.File;

public class Ex1 {
    public Ex1(){
        File temp;
        File a = new File(Globals.pathToFile);
        for (File file: a.listFiles()){ //file = a.listFiles()[i]
            System.out.println("Nom : " +file.getName());
            System.out.println("Taille : " + file.length());
            System.out.println("Chemin absolu : " + file.getAbsolutePath());
            if(file.isDirectory()){
                System.out.println("C'est un repertoire");
            }
            System.out.println("");
        }
    }

    public static void main(String[] args) {
        new Ex1();
    }
}
