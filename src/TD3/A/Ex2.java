package TD3.A;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Ex2 {
    private final String file;

    public Ex2(String file){
        this.file = file;
    }
    public double somme() throws IOException {
        double somme = 0;
        BufferedReader reader = new BufferedReader(new FileReader(Globals.pathToFile + file));
        String ligne;
        while((ligne = reader.readLine()) != null){
            StringTokenizer str = new StringTokenizer(ligne);
            while(str.hasMoreElements()){
                somme += Double.parseDouble(str.nextToken());
            }
        }
        return somme;
    }


    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Hop hop hop on ecrit");
        Ex2 a = new Ex2(scan.nextLine());
        System.out.println(a.somme());
    }
}
