package TD3.A;
import java.io.*;

public class Ex1 {
    private final String path;

    public Ex1(String path){
        this.path = path;
    }

    public void readText() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(path));
        String ligne = reader.readLine();
        while(ligne != null){
            System.out.println(ligne);
            ligne = reader.readLine();
        }
    }

    public static void main(String[] args) {
        Ex1 a = new Ex1(Globals.pathToFile + "text.txt");
        try{
            a.readText();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
