package TD3.A;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;

public class Ex5 extends JFrame implements ActionListener {
    private JTextArea t;
    private JButton b;
    public Ex5(){
        this.setBounds(200,200,1000,500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        Container fen = this.getContentPane();
        fen.setLayout(new GridLayout(3,1));
        JLabel l = new JLabel("Ecrire si dessous");
        fen.add(l);
        t = new JTextArea();
        fen.add(t);
        b = new JButton("Envoie !");
        b.addActionListener(this);
        fen.add(b);
        this.setVisible(true);
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            this.ecrire(this.t.getText());
        } catch (IOException ioException) {
            JOptionPane.showMessageDialog(this,"Pb d'écriture");
        }
    }

    private void ecrire(String text) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(Path.of(Globals.pathToFile + "Ex5.txt"));
        writer.write(text);
        writer.write("");
        this.t.setText("");
        writer.close();
    }

    public static void main(String[] args) {
        new Ex5();
    }
}
