package TD3.A;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Ex4 {
    private ArrayList<Double> list;
    private String file;
    public Ex4(){
        this.list = new ArrayList<Double>();
        Scanner scan = new Scanner(System.in);
        this.file = scan.nextLine();
    }
    public void getValue() throws IOException {
        String ligne = null;
        BufferedReader reader = new BufferedReader(new FileReader(Globals.pathToFile + this.file));
        while((ligne = reader.readLine()) != null){
            StringTokenizer str = new StringTokenizer(ligne);
            while(str.hasMoreElements()){
                list.add(Double.parseDouble(str.nextToken()));
            }
        }
    }
    public void ecriture() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter(Globals.pathToFile + "carre.txt"));
        writer.println("JE DONNE DES NOMBRES AU CARRE... ET TOI ? :)");
        for (Double el : list)
            writer.println(el + "   " + Math.pow(el, 2));
        writer.close();
    }

    public static void main(String[] args) throws IOException {
        Ex4 a = new Ex4();
        a.getValue();
        a.ecriture();
    }
}
