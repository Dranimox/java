package TD3.A;

import java.io.*;

public class Ex3 {
    private int[] grille;
    private final String path;

    public Ex3(String file) {
        grille = new int[6];
        this.path = Globals.pathToFile + file;
    }

    public void tirage() {
        int nbr = 0;
        boolean state = false;

        for (int j = 0; j < 6; j++) {
            do {
                state = false;
                nbr = 1 + (int) (Math.random() * 49);
                for (int pris : grille) { // verif
                    if (pris == nbr) {
                        state = true;
                        break;
                    }
                }
            } while (state);

            // nbr Ok
            grille[j] = nbr;
        }
    }

    public void ecriture() throws IOException {
        PrintWriter writer = new PrintWriter(new FileWriter(this.path));
        for (int i : grille)
            writer.println(i);
        writer.close();
    }

    public static void main(String[] args) {
        Ex3 a = new Ex3("loto.txt");
        a.tirage();
        try {
            a.ecriture();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
