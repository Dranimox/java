package TD2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class Ex3 extends JFrame {
    private final JCheckBoxMenuItem[] item_1;
    private final JCheckBoxMenuItem[] item_2;
    private final JCheckBoxMenuItem[] item_3;

    public JCheckBoxMenuItem[] getItem_1() {
        return item_1;
    }

    public JCheckBoxMenuItem[] getItem_2() {
        return item_2;
    }

    public JCheckBoxMenuItem[] getItem_3() {
        return item_3;
    }

    public Ex3() {
        this.setBounds(200,200,1000,500);
        Container fen = this.getContentPane();

        JMenuBar mb = new JMenuBar();

        JMenu apero = new JMenu("Apero");
        item_1 = new JCheckBoxMenuItem[6];
        for(int i=0; i<6; i++) {
            item_1[i] = new JCheckBoxMenuItem("plat" + i);
            apero.add(item_1[i]);
        }
        apero.add(apero);
        mb.add(apero);

        JMenu menu2 = new JMenu("Menu 2");
        item_2 = new JCheckBoxMenuItem[6];
        for(int i=0; i<6; i++) {
            item_2[i] = new JCheckBoxMenuItem("plat" + i);
            menu2.add(item_2[i]);
        }
        mb.add(menu2);

        JMenu menu3 = new JMenu("Menu 3");
        item_3 = new JCheckBoxMenuItem[6];
        for(int i=0; i<6; i++) {
            item_3[i] = new JCheckBoxMenuItem("plat" + i);
            menu3.add(item_3[i]);
        }
        mb.add(menu3);

        this.setJMenuBar(mb);
        this.setVisible(true);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
}


class Ex3Result extends JFrame implements ActionListener {
    private double price;
    private JLabel l;
    private JButton b;
    private final Ex3 menu;

    public Ex3Result(Ex3 menu){
        super();

        this.menu = menu;
        this.setBounds(500,500,1000,500);
        Container fen = this.getContentPane();
        l = new JLabel("Prix:");
        b = new JButton("Calcul prix");
        b.addActionListener(this);
        fen.add(l, BorderLayout.LINE_START);
        fen.add(b, BorderLayout.LINE_END);
        this.setVisible(true);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        JCheckBoxMenuItem[] item_1 = menu.getItem_1();
        JCheckBoxMenuItem[] item_2 = menu.getItem_2();
        JCheckBoxMenuItem[] item_3 = menu.getItem_3();


        for (JCheckBoxMenuItem box : item_1) {
            if (box.getState())
                price += 1;
            box.setState(false);
        }

        for (JCheckBoxMenuItem box2 : item_2) {
            if (box2.getState())
                price += 2;
            box2.setState(false);
        }

        for (JCheckBoxMenuItem box3 : item_3) {
            if (box3.getState())
                price += 3;
            box3.setState(false);
        }

        l.setText("Prix : "+ price + "€");
        price = 0;
    }

    public static void main(String[] args) {
        Ex3 menu = new Ex3();
        new Ex3Result(menu);
    }
}